const faker = require('faker');
const rubricData = require('./rubric.json');
const uuid = require('short-uuid');

module.exports = () => {
	const vidurl = ['','https://aslpi.s3.amazonaws.com/ytfiles/Walker Casler-CfweoeXvi5g.mp4','https://aslpi.s3.amazonaws.com/ytfiles/Dale Wolf-QJ4xxfqQptc.mp4','https://aslpi.s3.amazonaws.com/ytfiles/Jack Mahony-Y8FP1EuOH4g.mp4'];
	const vidname = ['','Julia Walker','Dale Gray','Jack Brown'];
	const data = {
		assessments: [],
	}
	const statuses =
		['active', 'pending', 'review', 'completed', 'inactive'];
	const roles = ['rater', 'admin', 'superuser']; 
	const grades =
		['1', '1+', '2', '2+', '3', '3+', '4', '4+', '5', '5+'];

// video_url: 'https://media.vimejs.com/720p.mp4',
// https://aslpi.s3.amazonaws.com/ytfiles/
	for (let i = 1; i < 25; i++) {
		const thisvid = (vidurl[i] == undefined ?  'https://media.vimejs.com/720p.mp4' : vidurl[i]);
		const thisname = (vidname[i] == undefined ? generateName() : vidname[i]);
		const raters = [{}, {}, {}];
		raters.forEach(rater => {
			rater.name = generateName();
			rater.assesment_status = pickElement(statuses);
			rater.grade = {
				pre: pickElementWithNull(grades),
				post: '',
			};
		});

		let assessment = {
			id: i,
			video_url: thisvid,
			video_upload_date: someTimeFuture(),
			assessment_type: 'ASLPI',
			minimum_annotations: faker.datatype.number({max: 15}),
			status: pickElement(statuses),
			grade: {
				pre: '',
				post: '',
			},
			comments: {
				text: "",
				video: ""
			},
			user: {
				id: uuid.generate(),
				name: generateName(),
				role: pickElement(roles),
				message: 'ignore dumb id',
			},
			raters: raters,
			interviewers: [{
				name: generateName(),
			}],
			uploader: {
				name: generateName(),
			},
			rubric: rubricData.rubric,
			annotations: [],
		};

		assessment.grade.post =
			setNullOrRandom(assessment.grade.pre, grades);
		assessment.raters.forEach(rater => {
			rater.grade.post =
				setNullOrRandom(rater.grade.pre, grades);
		});

		let annotations = [];
		const min_annotations = assessment.minimum_annotations;
		let annotations_length;
		const { grade, status } = assessment;
		if (grade.pre === null || status !== 'completed') {
			annotations_length =
				(min_annotations -
					faker.datatype.number({max: min_annotations}));
		} else {
			annotations_length = 
					faker.datatype.number({min: min_annotations, max: 25});
		}
		for (let i = 1; i <= annotations_length; i++) {
			let annotation = {
				id: uuid.generate(),
				range: {
					start: faker.datatype.number({min: 0, max: 100}),
					get end() {
						return faker.datatype.number({min: this.start, max: 200});
					},
				},
				message: "ignore my dumb IDs and dynamic end range",
				rating: [],
			}

			const rubric = assessment.rubric; 
			const rubric_length = rubric.length;
			const requiredRubricItems = rubric.filter(item =>  
				item.required != false
			);
			const unrequiredRubricItems = rubric.filter(item =>  
				item.required == false
			);
			const requiredRatings = processRatings(1, requiredRubricItems);
			const someRubricItems = pickElements(unrequiredRubricItems, 3);
			const unrequiredRatings = 
				processRatings(someRubricItems.length + 1, someRubricItems);
			let ratings = [...requiredRatings, ...unrequiredRatings];
			annotation.rating = ratings;
			annotations.push(annotation);
		}
		assessment.annotations = annotations;
		data.assessments.push(assessment);
	}
	return data;
}

function generateName() {
	return {
		first: faker.name.firstName(),
		last: faker.name.lastName(),
	}
}

function pickElement(elements) {
	return faker.random.arrayElement(elements);
}

function pickElements(elements, count) {
	return faker.random.arrayElements(elements, count);
}

function pickElementWithNull(elements) {
	const choices = [null, ...elements]
	return pickElement(choices);
}

function setNullOrRandom(property, choices) {
	return (property === null) ?
		null : pickElementWithNull([property, ...choices]);

}

function someTimeFuture() {
	return faker.date.soon(faker.datatype.number());
}

function processRatings(index, collection) {
	let ratings = [];
	for (let i = 0; i < collection.length; i++) {
		let rating = {
			id: uuid.generate(), // dumb IDs
			rubric_item_id: collection[i].id,
			rubric_item_title: collection[i].title,
			rubric_item_value: collection[i].value,
		}
		let randomOption = pickElement(collection[i].options);
		rating.rubric_item_option_value = randomOption.value;
		if (randomOption.options != undefined) {
			let randomOptionOption = 
				pickElementWithNull(randomOption.options);
			if (randomOptionOption != null) {
				rating.rubric_item_option_option_value = 
					randomOptionOption.value;
			}
		}
		ratings.push(rating);
	}
	return ratings;
}